package ru.ccfit.inechakhin.manager.database.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.ccfit.inechakhin.manager.database.entity.HashCrack;
import ru.ccfit.inechakhin.manager.database.entity.HashCrackStatus;

import java.util.List;

@Repository
public interface HashCrackRepository extends MongoRepository<HashCrack, String> {
    List<HashCrack> findByStatus(HashCrackStatus status);
}

package ru.ccfit.inechakhin.manager.facade;

import java.util.ArrayList;
import java.util.Collections;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ru.ccfit.inechakhin.manager.database.entity.HashCrack;
import ru.ccfit.inechakhin.manager.database.entity.HashCrackStatus;
import ru.ccfit.inechakhin.manager.service.HashCrackService;
import ru.ccfit.inechakhin.manager.web.dto.hash.HashCrackCreateDto;
import ru.ccfit.inechakhin.manager.web.dto.hash.HashCrackIdDto;
import ru.ccfit.inechakhin.manager.web.dto.hash.HashStatusDto;
import ru.nsu.ccfit.schema.crack_hash_request.CrackHashManagerRequest;

@Service
public class HashFacade {
    private final HashCrackService hashCrackService;
    private final CrackHashManagerRequest.Alphabet alphabet;
    @Value("${app.workerCount}")
    private Integer workerCount;

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public HashFacade(HashCrackService hashCrackService) {
        this.hashCrackService = hashCrackService;
        this.alphabet = new CrackHashManagerRequest.Alphabet();
        initAlphabet(this.alphabet);
    }

    private void initAlphabet(CrackHashManagerRequest.Alphabet alphabet) {
        for (int i = 0; i < 10; i++) {
            alphabet.getSymbols().add(String.valueOf((char) ('0' + i)));
        }
        for (int i = 0; i < 26; i++) {
            alphabet.getSymbols().add(String.valueOf((char) ('a' + i)));
        }
    }

    private void sendWorkToAllWorker(String requestId, String hash, Integer maxLenghth) throws Exception {
        try {
            for (int i = 0; i < workerCount; i++) {
                CrackHashManagerRequest crackHashManagerRequest = new CrackHashManagerRequest();
                crackHashManagerRequest.setRequestId(requestId);
                crackHashManagerRequest.setPartNumber(i);
                crackHashManagerRequest.setPartCount(workerCount);
                crackHashManagerRequest.setHash(hash);
                crackHashManagerRequest.setMaxLength(maxLenghth);
                crackHashManagerRequest.setAlphabet(alphabet);
                rabbitTemplate.convertAndSend("direct_exchange", "event_crack_hash_request", crackHashManagerRequest);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public HashCrackIdDto createHashCrack(HashCrackCreateDto hashCrackCreateRequest) {
        HashCrack createsHashCrack = new HashCrack();
        createsHashCrack.setHash(hashCrackCreateRequest.getHash());
        createsHashCrack.setMaxLength(hashCrackCreateRequest.getMaxLength());
        createsHashCrack.setListReady(new ArrayList<>(Collections.nCopies(workerCount, 0)));
        createsHashCrack.setStatus(HashCrackStatus.IN_PROGRESS);
        hashCrackService.saveHashCrack(createsHashCrack);
        String requestId = createsHashCrack.getHashCrackId();
        try {
            sendWorkToAllWorker(requestId, hashCrackCreateRequest.getHash(), hashCrackCreateRequest.getMaxLength());
        } catch (Exception e) {
            createsHashCrack.setStatus(HashCrackStatus.ERROR);
            hashCrackService.saveHashCrack(createsHashCrack);
        }
        HashCrackIdDto hashCrackIdDto = new HashCrackIdDto(requestId);
        return hashCrackIdDto;
    }

    private void resendAllErrorWork(String requestId) {
        ArrayList<HashCrack> errorList = (ArrayList<HashCrack>) hashCrackService.findHashCrackByStatus(HashCrackStatus.ERROR);
        for (HashCrack hashCrack : errorList) {
            try {
                sendWorkToAllWorker(requestId, hashCrack.getHash(), hashCrack.getMaxLength());
                hashCrack.setStatus(HashCrackStatus.IN_PROGRESS);
            } catch (Exception e) {
                hashCrack.setStatus(HashCrackStatus.ERROR);
            } finally {
                hashCrackService.saveHashCrack(hashCrack);
            }
        }
    }

    public HashStatusDto getHashCrackStatus(String requestId) {
        HashCrack hashCrack = hashCrackService.findHashCrackById(requestId);
        if (hashCrack.getStatus() == HashCrackStatus.ERROR) {
            // sendWorkToAllWorker(requestId, hashCrack.getHash(), hashCrack.getMaxLength());
            resendAllErrorWork(requestId);
        }
        HashStatusDto hashStatusDto = new HashStatusDto(hashCrack.getStatus().toString(), hashCrack.getListResult());
        return hashStatusDto;
    }
}

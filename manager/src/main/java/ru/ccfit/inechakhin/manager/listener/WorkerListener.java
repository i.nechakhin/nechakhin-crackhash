package ru.ccfit.inechakhin.manager.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import ru.ccfit.inechakhin.manager.facade.WorkerFacade;
import ru.nsu.ccfit.schema.crack_hash_response.CrackHashWorkerResponse;

@Component
public class WorkerListener {
    private final WorkerFacade workerFacade;

    public WorkerListener(WorkerFacade workerFacade) {
        this.workerFacade = workerFacade;
    }

    @RabbitListener(queues = "crack_hash_response")
    public void onMessageReceived(CrackHashWorkerResponse response) {
        workerFacade.setResultOfCrack(response);
    }

}

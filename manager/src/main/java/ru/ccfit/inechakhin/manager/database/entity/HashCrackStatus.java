package ru.ccfit.inechakhin.manager.database.entity;

public enum HashCrackStatus {
    IN_PROGRESS,
    READY,
    ERROR
}

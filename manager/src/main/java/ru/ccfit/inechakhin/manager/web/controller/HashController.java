package ru.ccfit.inechakhin.manager.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.ccfit.inechakhin.manager.facade.HashFacade;
import ru.ccfit.inechakhin.manager.web.dto.hash.HashCrackCreateDto;
import ru.ccfit.inechakhin.manager.web.dto.hash.HashCrackIdDto;
import ru.ccfit.inechakhin.manager.web.dto.hash.HashStatusDto;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/api/hash")
public class HashController {
    private final HashFacade hashFacade;

    public HashController(HashFacade hashFacade) {
        this.hashFacade = hashFacade;
    }

    @PostMapping("/crack")
    public ResponseEntity<HashCrackIdDto> createHashCrack(@RequestBody HashCrackCreateDto hashCrackForCreate) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(hashFacade.createHashCrack(hashCrackForCreate));
    }

    @GetMapping("/status")
    public ResponseEntity<HashStatusDto> getHashCrackStatus(
            @RequestParam(value = "requestId", required = true) String requestId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(hashFacade.getHashCrackStatus(requestId));
    }

}

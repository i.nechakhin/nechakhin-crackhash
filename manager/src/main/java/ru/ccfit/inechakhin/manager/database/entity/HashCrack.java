package ru.ccfit.inechakhin.manager.database.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class HashCrack {
    @Id
    private String hashCrackId;
    private String hash;
    private Integer maxLength;
    private HashCrackStatus status;
    private List<String> listResult;
    private List<Integer> listReady;

    public HashCrack() {
    }

    public String getHashCrackId() {
        return hashCrackId;
    }

    public void setHashCrackId(String hashCrackId) {
        this.hashCrackId = hashCrackId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public HashCrackStatus getStatus() {
        return status;
    }

    public void setStatus(HashCrackStatus status) {
        this.status = status;
    }

    public List<String> getListResult() {
        return listResult;
    }

    public void setListResult(List<String> listResult) {
        this.listResult = listResult;
    }

    public List<Integer> getListReady() {
        return listReady;
    }

    public void setListReady(List<Integer> listReady) {
        this.listReady = listReady;
    }
}

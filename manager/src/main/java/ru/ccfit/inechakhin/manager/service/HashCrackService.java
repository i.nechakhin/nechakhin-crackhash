package ru.ccfit.inechakhin.manager.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityNotFoundException;
import ru.ccfit.inechakhin.manager.database.entity.HashCrack;
import ru.ccfit.inechakhin.manager.database.entity.HashCrackStatus;
import ru.ccfit.inechakhin.manager.database.repository.HashCrackRepository;

@Service
public class HashCrackService {
    private final HashCrackRepository hashCrackRepository;

    public HashCrackService(HashCrackRepository hashCrackRepository) {
        this.hashCrackRepository = hashCrackRepository;
    }

    @Transactional
    public HashCrack saveHashCrack(HashCrack hashCrack) {
        return hashCrackRepository.save(hashCrack);
    }

    @Transactional(readOnly = true)
    public HashCrack findHashCrackById(String id) {
        return hashCrackRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("HashCrack not found"));
    }

    @Transactional(readOnly = true)
    public List<HashCrack> findHashCrackByStatus(HashCrackStatus status) {
        return hashCrackRepository.findByStatus(status);
    }
}

package ru.ccfit.inechakhin.manager.facade;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import ru.ccfit.inechakhin.manager.database.entity.HashCrack;
import ru.ccfit.inechakhin.manager.database.entity.HashCrackStatus;
import ru.ccfit.inechakhin.manager.service.HashCrackService;
import ru.nsu.ccfit.schema.crack_hash_response.CrackHashWorkerResponse;

@Service
public class WorkerFacade {
    private final HashCrackService hashCrackService;

    public WorkerFacade(HashCrackService hashCrackService) {
        this.hashCrackService = hashCrackService;
    }

    private boolean isReadyHashCrack(HashCrack hashCrack) {
        List<Integer> readyList = hashCrack.getListReady();
        for (Integer elem : readyList) {
            if (elem == 0) {
                return false;
            }
        }
        return true;
    }

    public void setResultOfCrack(CrackHashWorkerResponse crackHashWorkerResponse) {
        String requestId = crackHashWorkerResponse.getRequestId();
        Integer partNumber = crackHashWorkerResponse.getPartNumber();
        List<String> result = crackHashWorkerResponse.getAnswers().getWords();

        HashCrack hashCrack = hashCrackService.findHashCrackById(requestId);
        List<Integer> readyList = hashCrack.getListReady();
        readyList.set(partNumber, 1);
        if (isReadyHashCrack(hashCrack)) {
            hashCrack.setStatus(HashCrackStatus.READY);
        }
        List<String> list = hashCrack.getListResult();
        if (list == null) {
            hashCrack.setListResult(result);
        } else {
            hashCrack.setListResult(Stream.concat(list.stream(), result.stream()).toList());
        }
        hashCrackService.saveHashCrack(hashCrack);
    }
}

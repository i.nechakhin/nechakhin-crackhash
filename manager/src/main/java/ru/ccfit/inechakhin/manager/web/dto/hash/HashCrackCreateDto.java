package ru.ccfit.inechakhin.manager.web.dto.hash;

public class HashCrackCreateDto {
    private String hash;
    private Integer maxLength;

    public HashCrackCreateDto(String hash, Integer maxLength) {
        this.hash = hash;
        this.maxLength = maxLength;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((hash == null) ? 0 : hash.hashCode());
        result = prime * result + ((maxLength == null) ? 0 : maxLength.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HashCrackCreateDto other = (HashCrackCreateDto) obj;
        if (hash == null) {
            if (other.hash != null)
                return false;
        } else if (!hash.equals(other.hash))
            return false;
        if (maxLength == null) {
            if (other.maxLength != null)
                return false;
        } else if (!maxLength.equals(other.maxLength))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "HashCrackCreateDto [hash=" + hash + ", maxLength=" + maxLength + "]";
    }
}

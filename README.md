# Nechakhin-CrackHash
Distributed system for brute-force hacking MD5 hash
### Using   
To run the service you need a swarm mode on Docker.
Easy way to enable it:
```
docker swarm init
```
Before the first start, run:
```
docker-compose -f docker-compose.yaml up
docker-compose -f docker-compose.yaml down --volumes
```
To run the service:
```
docker stack deploy --compose-file docker-compose.yaml crackhash
```
To stop the service:
```
docker stack rm crackhash
```
To leave from a swarm mode:
```
docker swarm leave -f
```

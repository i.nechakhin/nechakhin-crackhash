package ru.ccfit.inechakhin.worker.listener;

import java.io.IOException;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

import ru.ccfit.inechakhin.worker.facade.ManagerFacade;
import ru.nsu.ccfit.schema.crack_hash_request.CrackHashManagerRequest;

@Component
public class ManagerListener {
    private final ManagerFacade managerFacade;

    public ManagerListener(ManagerFacade managerFacade) {
        this.managerFacade = managerFacade;
    }

    @RabbitListener(queues = "crack_hash_request")
    public void onMessageReceived(
            CrackHashManagerRequest request,
            @Header(AmqpHeaders.CHANNEL) Channel channel,
            @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        try {
            managerFacade.bruteforceCrackHash(request);
            channel.basicAck(tag, false);
        } catch (Exception e) {
            channel.basicReject(tag, true);
        }
    }
}

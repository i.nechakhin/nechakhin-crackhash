package ru.ccfit.inechakhin.worker.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MarshallingMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class RabbitMQConfig {
    @Value("${spring.rabbitmq.virtual-host}")
    private String virtualHost;
    @Value("${spring.rabbitmq.host}")
    private String host;

    @Bean
    Queue queueCrackHashRequest() {
        return new Queue("crack_hash_request", true, false, false);
    }

    @Bean
    Queue queueCrackHashResponse() {
        return new Queue("crack_hash_response", true, false, false);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange("direct_exchange", true, false);
    }

    @Bean
    Binding bindingCrackHashRequest(Queue queueCrackHashRequest, DirectExchange exchange) {
        return BindingBuilder.bind(queueCrackHashRequest).to(exchange).with("event_crack_hash_request");
    }

    @Bean
    Binding bindingCrackHashResponse(Queue queueCrackHashResponse, DirectExchange exchange) {
        return BindingBuilder.bind(queueCrackHashResponse).to(exchange).with("event_crack_hash_response");
    }

    @Bean
    public Jaxb2Marshaller xmlMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(new Class[] {
                ru.nsu.ccfit.schema.crack_hash_request.CrackHashManagerRequest.class,
                ru.nsu.ccfit.schema.crack_hash_response.CrackHashWorkerResponse.class
        });
        return marshaller;
    }

    @Bean
    public MarshallingMessageConverter xmlMessageConverter() {
        MarshallingMessageConverter converter = new MarshallingMessageConverter();
        converter.setMarshaller(xmlMarshaller());
        converter.setUnmarshaller(xmlMarshaller());
        return converter;
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setVirtualHost(virtualHost);
        connectionFactory.setHost(host);
        return connectionFactory;
    }

    @Bean
    public AmqpTemplate rabbitTemplateCrackHashResponse(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(xmlMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {
        final SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setMessageConverter(xmlMessageConverter());
        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        return factory;
    }
}

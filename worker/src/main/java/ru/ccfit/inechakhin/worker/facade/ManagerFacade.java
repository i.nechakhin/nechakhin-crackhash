package ru.ccfit.inechakhin.worker.facade;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringUtils;
import org.paukov.combinatorics.CombinatoricsFactory;

import ru.nsu.ccfit.schema.crack_hash_request.CrackHashManagerRequest;
import ru.nsu.ccfit.schema.crack_hash_response.CrackHashWorkerResponse;

@Service
public class ManagerFacade {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    private String md5(String value) {
        MessageDigest md;
        String hash = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(value.getBytes());
            byte[] digest = md.digest();
            hash = DatatypeConverter.printHexBinary(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hash;
    }

    public void bruteforceCrackHash(CrackHashManagerRequest crackHashManagerRequest) {
        // get params
        List<String> alphabet = crackHashManagerRequest.getAlphabet().getSymbols();
        int maxLength = crackHashManagerRequest.getMaxLength();
        int partNumber = crackHashManagerRequest.getPartNumber();
        int partCount = crackHashManagerRequest.getPartCount();
        String hash = crackHashManagerRequest.getHash();
        // brute-force
        CrackHashWorkerResponse.Answers answers = new CrackHashWorkerResponse.Answers();
        int index = 0;
        ICombinatoricsVector<String> vector = CombinatoricsFactory.createVector(alphabet);
        for (int i = 1; i <= maxLength; i++) {
            Generator<String> gen = CombinatoricsFactory.createPermutationWithRepetitionGenerator(vector, i);
            for (ICombinatoricsVector<String> perm : gen) {
                if (index % partCount == partNumber) {
                    String value = StringUtils.join(perm.getVector(), "");
                    String calcHash = md5(value).toLowerCase();
                    if (calcHash.equals(hash)) {
                        answers.getWords().add(value);
                    }
                }
                index++;
            }
        }
        // send answers
        CrackHashWorkerResponse crackHashWorkerResponse = new CrackHashWorkerResponse();
        crackHashWorkerResponse.setRequestId(crackHashManagerRequest.getRequestId());
        crackHashWorkerResponse.setPartNumber(partNumber);
        crackHashWorkerResponse.setAnswers(answers);
        rabbitTemplate.convertAndSend("direct_exchange", "event_crack_hash_response", crackHashWorkerResponse);
    }
}
